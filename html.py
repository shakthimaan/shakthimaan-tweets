#!/usr/bin/python

import datetime
import re

print '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!-- head -->
<head>
  <title>/links/documentation</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="http://www.shakthimaan.com/css/gnu.css" />
  <link rel="stylesheet" type="text/css" href="http://www.shakthimaan.com/css/first.css" />
  <script type="text/javascript" src="http://www.shakthimaan.com/main.js"></script>
</head>

<!-- body -->
<body>

<!-- header links -->
<p class="links">
    <script type="text/javascript">
      //<![CDATA[
      for (var i=0; i<header_links.length; i++)
	document.write (header_links[i] + " ");
      //]]>
    </script>
</p>

<div class="trademark">tweets</div>
<h3 class="header"></h3>

<div class="menu">

    <script type="text/javascript">
      //<![CDATA[
      var full_path = window.location.pathname;
      var file_ext = full_path.substring(full_path.lastIndexOf('/')+1);
      var file_name = file_ext.substring(0, file_ext.lastIndexOf('.'));
      for (var i=0; i<links_links.length; i++)
        {
        if (links_links[i]==file_name)
	document.write (links_links[i] + "<br />");
        else
	document.write ("<a href=\\"http://www.shakthimaan.com/links/" + links_links[i] + ".html\\">" + links_links[i] + "</a><br />");
        }
      //]]>
    </script>
</div>
'''

print '''
<a href="http://identi.ca/shakthimaan">
<img src="http://shakthimaan.com/downloads/companylogos/identi_ca.jpg" alt="identica logo">
</img>
</a>
<a href="http://twitter.com/shakthimaan">
<img src="http://shakthimaan.com/downloads/companylogos/twitter.png" alt="twitter logo">
</img>
</a>
'''

print "<p>Last updated: ", datetime.datetime.now().strftime("%A, %d, %B %Y %I:%M%p IST"), "</p>"

print "<table  style=\"width: auto; border: 0\" cellspacing=\"15\">"

f = open("complete.txt", 'r')

while True:
    line1 = line2 = line3 = ""
    line1 = f.readline().rstrip()
    if not line1:
        break
    line1 = re.sub(r'(https?:[^\s\n\r]+)', r'<a href="\1">\1</a>', line1)
    line2 = f.readline().rstrip()
    line3 = f.readline()
    print "<tr>"
    print "<td>", line1
    print "</td>"
    print "<td width=\"25%\">", line2
    print "</td>"
    print "</tr>"

print "</table></body>"
print '''
<!-- footer -->
<p class="footer">
    <script type="text/javascript">
      document.write (footer);
    </script>
</p>
'''
print "</html>"

f.close()

