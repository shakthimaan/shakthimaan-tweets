{--

 Copyright (C) 2011 Shakthi Kannan
 
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the license, or (at your
 option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
 the full text of the license.

 Email: shakthimaan at gmail dot com

--}

import System.IO
import System.Time
import Text.Regex

dumpFile :: FilePath -> IO ()
dumpFile f = do
         contents <- readFile f
         putStr contents

dumpTimeStamp :: IO ()
dumpTimeStamp = do
     date <- getClockTime
     putStrLn "<p>Last updated: "
     putStrLn $ show date 
     putStrLn "</p>"

getTweets :: Handle -> Int -> IO ()
getTweets handle count = do
          t <- hIsEOF handle
          if t then return()
          else if count <= 0 then return ()
             else do
                  tweet <- hGetLine handle
                  date <- hGetLine handle
                  empty <- hGetLine handle
                  putStrLn "<tr>\n<td>"
                  putStrLn $ subRegex (mkRegex "https?[^[:space:]]+")
                           tweet "<a href=\"\\0\">\\0</a>" 
                  putStrLn "</td>\n<td style=\"width:150px; white-space:nowrap;\">"
                  putStrLn date
                  putStrLn "</td>\n</tr>"
                  getTweets handle (count - 1)

main = do
     dumpFile "data/header.html"
     dumpTimeStamp
     dumpFile "data/table-start.html"
     handle <- openFile "complete.txt" ReadMode
     getTweets handle 10
     hClose handle
     dumpFile "data/table-end.html"

