TARGET=html

all:
	ghc --make $(TARGET).hs -o $(TARGET)

send: all
	./html > /tmp/tweets.html
	make clean
	sh ~/daily/scripts/update-tweets.sh

clean:
	rm -f *~ *.o *.hi $(TARGET)

